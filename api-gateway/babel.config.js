const babel = {
  plugins: [
    [
      "modulo-resolver",
      {
        alias: {
          "#root": "./src",
        },
      },
    ],
  ],
  presets: [
    [
      "@babel/preset-env",
      {
        targets: {
          node: "current",
        },
      },
    ],
  ],
};
