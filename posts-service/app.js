import express, { json, urlencoded } from "express";
import cors from "cors";
import { config } from "dotenv";
import mongoose from "mongoose";

import router from "./routes/index.js";
import dbConnection from "./database/config.js";

const port = process.env.PORT || 5000;

const app = () => {
  try {
    const appServer = express();
    config();

    //Serializacion de informacion a json
    appServer.use(json({ limit: "30mb", extendend: true }));

    appServer.use(urlencoded({ extendend: true }));

    const corsOptions = {
      origin: "*",
      optionsSucessStatus: 200,
    };

    appServer.use(cors(corsOptions));

    appServer.use("/", router);

    appServer.listen(port, () => {
      console.log(`Server running on port: ${port}`);
    });
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }

  return app;
};

const expressApp = async () => {
  await dbConnection()
    .then(() => {
      app();
    })
    .catch((error) => {
      console.log(error);
    });
};

export default expressApp;
