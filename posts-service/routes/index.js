import express from "express";
import path from "path";
import { fileURLToPath } from "url";
import postsRoutes from "./posts.js";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const router = express.Router();

router.get("/", (req, res) => {
  res.send("Hola, este es un microservicio con NodeJS/Express/Docker");
});

router.use("/posts", postsRoutes);

router.use("*", (req, res) => {
  let url = __dirname.replace("routes", "") + "public/404.html";
  console.log(url);
  res.status(404);
  //res.send("404 | Not Found");
  res.sendFile(url);
});

export default router;
