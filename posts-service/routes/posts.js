import express from "express";

import {
  getPosts,
  createPost,
  updatePost,
  deletePost,
  likePost,
} from "../controllers/posts.js";

const postsRoutes = express.Router();

postsRoutes.get("/", getPosts);
postsRoutes.post("/", createPost);
postsRoutes.patch("/:id", updatePost);
postsRoutes.delete("/:id", deletePost);
postsRoutes.patch("/:id/likePost", likePost);

export default postsRoutes;
