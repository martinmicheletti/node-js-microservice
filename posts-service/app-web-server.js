import http from "http";

import express, { json, urlencoded } from "express";
import cors from "cors";
import { config } from "dotenv";

import userRoutes from "./routes/users.js";
import weatherRoutes from "./routes/weather.js";

import swaggerUi from "swagger-ui-express";
import swaggerDocument from "./swagger.json";
config();

const port = process.env.SERVER_PORT || 5000;

const server = (req, res) => {
  res.writeHead(200);
  res.write("Hola WebServer HTTP");
  res.end();
};

http.createServer(server).listen(port);

console.log("Server online on port " + port);
