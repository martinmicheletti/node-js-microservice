class Result {
  ok = false;
  statusCode = 200;
  data = {};
  error = "";

  constructor() {}

  setOK(data) {
    this.ok = true;
    this.statusCode = 200;
    this.data = data;
    this.error = "";
  }

  setError(err, statusCode) {
    this.ok = false;
    this.statusCode = statusCode;
    this.data = {};
    this.error = err;
  }
}

export default Result;
